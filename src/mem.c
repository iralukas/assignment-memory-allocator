#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool heap_inited;
static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/* Allocate memory and initialize block*/
static struct region alloc_region  ( void const * addr, size_t query ) {
  query = region_actual_size(query);
  struct region region = (struct region){.size = query, .extends = true};
  void* mmap_addr = map_pages(addr, query, MAP_FIXED_NOREPLACE); // try to allocate closely

  if (mmap_addr == MAP_FAILED){
    mmap_addr = map_pages(addr, query, 0); // try to allocate anywhere
    if (mmap_addr == MAP_FAILED) return REGION_INVALID;
    }

  region.addr = mmap_addr;
  block_init(mmap_addr, (block_size){query}, NULL); // initialize the first block
  return region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  heap_inited = true;
  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  Split block if it is too big */
static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  query = size_max(query, BLOCK_MIN_CAPACITY); // check if its too small
  if (block && block_splittable(block, query)) {
      struct block_header *new_header = (struct block_header*)(block -> contents + query);
      block_init(new_header, (block_size){.bytes = block -> capacity.bytes - query}, block -> next);
      block -> next = new_header;
      block -> capacity.bytes = query;
    return true;
  }
  return false;
}


/*  Merge two blocks */
static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}

/* Check if snd block comes after fst */
static bool blocks_continuous ( 
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst); 
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (block && (block -> next) && mergeable(block, block -> next)) { // if both blocks exist and can be merged
      block -> capacity.bytes = block->capacity.bytes + size_from_capacity(block->next->capacity).bytes;
      block -> next = block -> next -> next;
      return true;
  }
  return false;
}

/*  If heap size is enough */
struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    while (block) {
        while (try_merge_with_next(block));
        if (block->is_free && block_is_big_enough(sz, block)) {
            split_if_too_big(block, sz);
            return (struct block_search_result) {.type=BSR_FOUND_GOOD_BLOCK, .block=block};
        }
        if (!block->next) return (struct block_search_result) {.type=BSR_REACHED_END_NOT_FOUND, .block=block};
        else block = block->next;
    }
    return (struct block_search_result) {.type=BSR_CORRUPTED, .block=NULL};
}

/*  Try to memalloc existing. Can be reused as soon as the heap is expanded */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK && result.block) result.block -> is_free = false;
  return result;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  struct block_header* new_header = (struct block_header*) alloc_region(block_after(last), query).addr;
    if (!new_header) return NULL;
    last -> next = new_header;
    return new_header;
}

/*  Implements the basic logic of malloc. Returns the header of the block*/
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (!heap_start) return NULL;
  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result =  try_memalloc_existing(query, heap_start);

  if (result.type == BSR_REACHED_END_NOT_FOUND) {
      if (grow_heap(result.block, query)) result = try_memalloc_existing(query, result.block);
      else return NULL; 
  }
  return result.block;

}

void* _malloc( size_t query ) {
  if (!heap_inited) heap_init(query);
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) {
    addr -> is_free = false; // mark as taken
    return addr->contents;
  }
  else return NULL;
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free(void* mem) {
  if (!mem) return;
  struct block_header* header = block_get_header(mem);
  header->is_free = true;
  try_merge_with_next(header); 
}
