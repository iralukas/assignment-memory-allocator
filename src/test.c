#include "test.h"

#include "mem.h"
#include "util.h"


bool alloc_test(){
    bool status = false;
    fprintf(stderr, "\nTEST 1: Usual memory allocation\n");
    void* block_1 = _malloc(2);
    void* block_2 = _malloc(512); 
    if (block_2 && block_1 && block_get_header(block_1)->capacity.bytes==24 && block_get_header(block_2)->capacity.bytes==512){
        fprintf(stderr, "TEST 1 PASSED\n");
        status = true;
    }
    else fprintf(stderr, "TEST 1 FAILED\n"); 
    _free(block_1);
    _free(block_2);
    return status;
}

bool free_one_test(){
    bool status = false;
    fprintf(stderr,"\nTEST 2: Try to free one block\n");
    void* block_1 = _malloc(400);  
    _free(block_1);
    if (block_get_header(block_1)->is_free){
        fprintf(stderr, "TEST 2 PASSED\n");
        status = true;
    }
    else fprintf(stderr, "TEST 2 FAILED\n"); 
    return status;
}

bool free_two_test(){
    bool status = false;
    fprintf(stderr, "\nTEST 3: Try to free two blocks\n");
    void* block_1 = _malloc(400); 
    void* block_2 = _malloc(400);
    _free(block_1);
    _free(block_2);
    if (block_get_header(block_1)->is_free && block_get_header(block_2)->is_free){
        fprintf(stderr,"TEST 3 PASSED\n");
        status = true;
    }
    else fprintf(stderr, "TEST 3 FAILED\n");
    return status;
}

bool grow_heap_test(){ 
    bool status = false;
    fprintf(stderr,"\nTEST 4: Try to expand the heap\n");
    void *block_1 = _malloc(400);
    void *block_2 = _malloc(64000);
    if (block_get_header(block_1)->capacity.bytes==400 && block_get_header(block_2)->capacity.bytes==64000){
        fprintf(stderr,"TEST 4 PASSED\n");
        status = true;
    }
    else fprintf(stderr,"TEST 4 FAILED\n");
    _free(block_1);
    _free(block_2);
    return status;
}

bool grow_heap_somewhere_test(){
    bool status = false;
    printf("\nTEST 5: Try to expand the heap with external region\n");
    struct block_header* header = (struct block_header*) _malloc(400);
    while (header->next) header=header->next;
    map_pages((uint8_t *) header + size_from_capacity(header->capacity).bytes, 1000, MAP_FIXED);
    void* block = _malloc(10000);
    struct block_header* block_header = block_get_header(block);
    if(block_header !=header) {
        fprintf(stderr, "TEST 5 PASSED\n\n");
        status = true;
    }
    else fprintf(stderr, "TEST 5 FAILED\n\n");
    _free(block);
    _free(header);
    return status;
}

int8_t all_tests(){
    int8_t result = 0;
    if (alloc_test()==true) result+=1;
    if (free_one_test()==true) result += 1;
    if (free_two_test()==true) result += 1;
    if (grow_heap_test()==true) result += 1;
    if (grow_heap_somewhere_test()==true) result += 1;
    if (result != 5) return 1;
    fprintf(stdout, "All tests done, passed: %d, failed: %d\n", result, 5-result);
    return 0;
}