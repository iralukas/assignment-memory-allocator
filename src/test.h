#ifndef TEST_H
#define TEST_H

#include "mem_internals.h"

int8_t all_tests();
bool alloc_test();
bool free_one_test();
bool free_two_test();
bool grow_heap_test();
bool grow_heap_somewhere_test();

#endif